/* Lee un tga de 24 bits, hace un proceso b�sico y escribe un tga out.tga      */
/* out.tga se puede visualizar con photoshop                                   */
/* Para generar un tga, con photoshop coger cualquier imagen, y guardarla como */
/* tga a 24 bits sin compresion, a menor resolucion de 1200x1200               */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define SALIDA "out.tga"            // el archivo de salida
#define MAXRX 2048                 // el tama�o m�ximo del array en x
#define MAXRY 2048                  // y en y

#define degreesToRadians(angleDegrees) ((angleDegrees) * M_PI / 180.0)

unsigned char Img[MAXRX][MAXRY][3];  // aqui se guarda la imagen, tres bytes R, G, B
                                     // Un array es muchisimo m�s r�pido que un array de estructuras
                                     // o una estructura de tres arrays
int Rx,Ry;                           // la Resoluci�n de la imagen

unsigned char R[MAXRX][MAXRY][3];
unsigned char Img2[MAXRX][MAXRY][3];
char na[100];
char res[100];

 
 int fin = 0; 						//Flujo de control principal

//------Funciones Auxiliares------

//Salir del programa
void salir(){
	fin = 1;
}

void limpiarImagen(){
	int y,x=0;
	for (y=0;y<Ry;y++){
  		for (x=0;x<Rx;x++){
  			R[x][y][0]=0;
  			R[x][y][1]=0;
  			R[x][y][2]=0;
  		}
	}
}
//calcula el valor minimo de un canal de la imagen
int minimo(int n){

	int x,y,z;
	int min = 9999;
	
	for (y=0;y<Ry;y++){
  		for (x=0;x<Rx;x++){
  			if(Img[x][y][n] < min)
  			min = Img[x][y][n]; 
		}
	}
	return min;
}
//calcula el maximo entre 2 valores
int maxnum(int n, int m){
	if(n>=m) return n;
	else return m;
}
//calcula el valor maximo de un canal
int maximo(int n){

	int x,y,z;
	int max = 0;
	
	for (y=0;y<Ry;y++){
  		for (x=0;x<Rx;x++){
  			if(Img[x][y][n] > max)
  			max = Img[x][y][n]; 
			  
		}
	}
	return max;
	
}
//Devuelve el nombre de una imagen sin extension
char* sinExt(char na[]){
	
	strcpy(res,na);
	res[strlen(res)-4]='\0';
	return res;
}


void LeeTga(char na[])               // Lee un tga 24 de bits.
{
 FILE *fe;
 int x,y,i;
 unsigned char c,resto,cociente;
 
 fe=fopen(na,"rb");
 if (fe==NULL)
   {printf("Error al abrir archivo %s\nPulsa...",na); getchar();exit(1);}

 for(i=0;i<12;i++)                              //leo 12 bytes de la cabecera que no se usan para nada
   c=fgetc(fe);
   
 resto=fgetc(fe);                               //La resolucion se codifica en dos bytes
 cociente=fgetc(fe);                            //el primero es el resto de dividir por 256
 Rx=cociente*256+resto;                         //y el segundo el cociente. Y lo recompongo

 resto=fgetc(fe);                               // igual para la Ry
 cociente=fgetc(fe);                            
 Ry=cociente*256+resto;

 c=fgetc(fe);
 c=fgetc(fe);                                   //los ultimos 2 bytes de la cabecera
 
 for(y=0;y<Ry;y++)                              //aqui leo la imagen
  for(x=0;x<Rx;x++)
  {
    Img[x][y][2]=fgetc(fe);                     //lee en orden B, G, R
    Img[x][y][1]=fgetc(fe);
    Img[x][y][0]=fgetc(fe);
  }

  fclose(fe);
}


void GuardaTga(char subfijo[])                                // Escribe un TGA guardado en memoria, a 24 bits
{
 FILE *fs;
 int x,y;
 
 fs=fopen(strcat(sinExt(na),subfijo),"w+b");                        //abro para escritura binaria
 if (fs==NULL)
   {printf("Error al crear archivo %s\nPulsa...",SALIDA); getch();exit(1);}

 fputc(0,fs); /* 0 */                          // guardo valores de cebecera: la mayoria son ceros
 fputc(0,fs); /* 1 */
 fputc(2,fs); /* 2 */
 fputc(0,fs); /* 3 */
 fputc(0,fs); /* 4 */
 fputc(0,fs); /* 5 */
 fputc(0,fs); /* 6 */
 fputc(0,fs); /* 7 */
 fputc(0,fs); /* 8 */
 fputc(0,fs); /* 9 */
 fputc(0,fs); /* 10 */
 fputc(0,fs); /* 11 */
 fputc(Rx%256,fs); /* 12 */                   //La resolucion x en dos bytes
 fputc(Rx/256,fs); /* 13 */
 fputc(Ry%256,fs); /* 14 */                   //la resolucion y en dos bytes
 fputc(Ry/256,fs); /* 15 */
 fputc(24,fs); /* 16 */                       //indica 24 bits por pixel
 fputc(0,fs); /* 17 */                        // acabo la cabecera

 for(y=0;y<Ry;y++)                            // y aqui guardo la imagen por filas horizontales
  for(x=0;x<Rx;x++)
  {
    fputc((char)R[x][y][2],fs);             //tres bytes por pixel, B, G, R
    fputc((char)R[x][y][1],fs);
    fputc((char)R[x][y][0],fs);
  }

  fclose(fs);
}


//------------------------Transformaciones de aritm�tica b�sicas------------------------------------------

//OK
//Suma un valor val a todos los p�xeles de la matriz original,  y lo almacena en una matriz resultante R
void suma(int val){
	int x,y;
	int r,g,b;
	for (y=0;y<Ry;y++){
	
  		for (x=0;x<Rx;x++){
    		r=Img[x][y][0];
			g=Img[x][y][1];
			b=Img[x][y][2];
    		r=r+val;
    		g=g+val;
    		b=b+val;
    		if(r > 255) r= 255;
    		if(g > 255) g= 255;
    		if(b > 255) b= 255;
  			R[x][y][0]= r;
			R[x][y][1]= g;
			R[x][y][2]= b;
   		}
   }
}

//OK
//Resta un valor val a todos los p�xeles de la matriz original,  y lo almacena en una matriz resultante R
void resta(int val){
	int x,y;
	int r,g,b;
	for (y=0;y<Ry;y++){
	
  		for (x=0;x<Rx;x++){
    		r=Img[x][y][0];
			g=Img[x][y][1];
			b=Img[x][y][2];
    		r=r-val;
    		g=g-val;
    		b=b-val;
    		if(r < 0) r= 0;
    		if(g < 0) g= 0;
    		if(b < 0) b= 0;
  			R[x][y][0]= r;
			R[x][y][1]= g;
			R[x][y][2]= b;
   		}
   }
}

//OK
//Multiplica un valor val a todos los p�xeles de la matriz original,  y lo almacena en una matriz resultante R
void multiplica(int val){
	int x,y;
	int r,g,b;
	for (y=0;y<Ry;y++){
  		for (x=0;x<Rx;x++){
    		r=Img[x][y][0];
			g=Img[x][y][1];
			b=Img[x][y][2];
    		r=r*val;
    		g=g*val;
    		b=b*val;
    		if(r > 255) r= 255;
    		if(g > 255) g= 255;
    		if(b > 255) b= 255;
  			R[x][y][0]= r;
			R[x][y][1]= g;
			R[x][y][2]= b;
   		}
   }
}

//OK
//Divide un valor val a todos los p�xeles de la matriz original,  y lo almacena en una matriz resultante R
void divide(int val){
	int x,y;
	int r,g,b;
	for (y=0;y<Ry;y++){
	
  		for (x=0;x<Rx;x++){
    		r=Img[x][y][0];
			g=Img[x][y][1];
			b=Img[x][y][2];
    		r=r/val;
    		g=g/val;
    		b=b/val;
    		if(r > 255) r= 255;
    		if(g > 255) g= 255;
    		if(b > 255) b= 255;
  			R[x][y][0]= r;
			R[x][y][1]= g;
			R[x][y][2]= b;
   		}
	}
}

//OK
//Invierte los colores de una matriz y lo almacena en una matriz resultante R
void invierte(){
	int y,x;
	for (y=0;y<Ry;y++){
	
  		for (x=0;x<Rx;x++){
  			
    		R[x][y][0] = 255 - Img[x][y][0];
			R[x][y][1] = 255 - Img[x][y][1];
			R[x][y][2] = 255 - Img[x][y][2];
    		
   		}
	}
}

//OK
void ajusteLineal(){
	int x,y;
	int minR,maxR,minG,maxG,minB,maxB;
	
	minR = minimo(0);
	maxR = maximo(0);
	minG = minimo(1);
	maxG = maximo(1);
	minB = minimo(2);
	maxB = maximo(2);	
	for (y=0;y<Ry;y++){
  		for (x=0;x<Rx;x++){
		    R[x][y][0] = ((Img[x][y][0]-minR)*255)/(maxR-minR);
		  	R[x][y][1] = ((Img[x][y][1]-minG)*255)/(maxG-minG);
			R[x][y][2] = ((Img[x][y][2]-minB)*255)/(maxB-minB);
		  }
	}
}


//KO
void ajusteNoLineal(){
	
/**	int x,y;
	
	for (y=0;y<Ry;y++){
  		for (x=0;x<Rx;x++){
  			
  			if(Img[x][y][0] < 128)
  				R[x][y][0] = pow((Img[x][y][0]),2.0) + Img[x][y][0];	
			else
				R[x][y][0] = powf(Img[x][y][0],0.5);


  			if(Img[x][y][1] < 128)
  				R[x][y][1] = pow((Img[x][y][1]),2.0) + Img[x][y][1];	
			else
				R[x][y][1] = powf(Img[x][y][1],0.5);
	
	
  			if(Img[x][y][2] < 128)
  				R[x][y][2] = pow((Img[x][y][2]),2.0) + Img[x][y][2];	
			else
				R[x][y][2] = powf(Img[x][y][2],0.5);
		
			if(R[x][y][0] > 255)  R[x][y][0]= 255;
			if(R[x][y][1] > 255)  R[x][y][1]= 255;
			if(R[x][y][2] > 255)  R[x][y][2]= 255;
				
  		}
  	} **/
}


//OK
//Tranformacion de la gama
void transformacionGama(float gama){
	
	int y,x;
	for (y=0;y<Ry;y++){
  		for (x=0;x<Rx;x++){	
			R[x][y][0] = 255*powf(Img[x][y][0]/255.0,(1.0/gama));
			R[x][y][1] = 255*powf(Img[x][y][1]/255.0,(1.0/gama));
			R[x][y][2] = 255*powf(Img[x][y][2]/255.0,(1.0/gama));
   		}
	}
	
}

//OK
//Ecualiza el histograma
void ecualizacion(){
	
	float fR[255]; // Funcion para el canal R
	float fG[255]; // Funcion para el canal G
	float fB[255]; // Funcion para el canal B
	
	int histR[255]; //Histograma de R
	int histG[255]; //Histograma de G
	int histB[255]; //Histograma de B
	
	int x,y,i;	//Variables de control de flujo
	long acumR;  //Valor acumulado de R
	long acumG;  //Valor acumulado de G
	long acumB;  //Valor acumulado de B
	long np = Rx * Ry;  // Valor total de numero de p�xeles
	
	
	for(i=0;i<255;i++){
		histR[i]=0;
		histG[i]=0;
		histB[i]=0;
		fR[i]=0;
		fG[i]=0;
		fB[i]=0;
	}
	
	//Calculo del histrograma de cada canal
	for (y=0;y<Ry;y++){
  		for (x=0;x<Rx;x++){
			histR[Img[x][y][0]]++;
			histG[Img[x][y][1]]++;
			histB[Img[x][y][2]]++;
			
			
		}
	}
	
	//Inicializaci�n de los valores acumulados de cada canal
	acumR = histR[0];
	acumG = histG[0];
	acumB = histB[0];


	for(i=1;i<255;i++){
		fR[i] = acumR*255.0/np;
		fG[i] = acumG*255.0/np;
		fB[i] = acumB*255.0/np;
		
		acumR = acumR + histR[i];
		acumG = acumG + histG[i];
		acumB = acumB + histB[i];
	}
	
	fR[255] =255;
	fG[255] =255;
	fB[255] =255;


	//Aplicaci�n de la funcion de ecualizacion a la matriz principal Img
	// y almacenamiento en la matriz resultante R
	for (y=0;y<Ry;y++){
  		for (x=0;x<Rx;x++){

			R[x][y][0] = fR[Img[x][y][0]];
			R[x][y][1] = fG[Img[x][y][1]];
			R[x][y][2] = fB[Img[x][y][2]];		 
		}
	}
}

//OK
//Umbralizacion
void umbral(int n){
	
	int y,x,r,g,b,med;
	if(n!= 0){
		for (y=0;y<Ry;y++){
  			for (x=0;x<Rx;x++){
  				r=Img[x][y][0];g=Img[x][y][1];b=Img[x][y][2];
    			med=(r+g+b)/3;
				if(med < n){
					R[x][y][0]=Img[x][y][0];
					R[x][y][1]=Img[x][y][1];
					R[x][y][2]=Img[x][y][2];
				}
				else
				  	R[x][y][0]=R[x][y][1]=R[x][y][2]=255;		
			  }
		  }		
	}
	else{
		for (y=0;y<Ry;y++){
  			for (x=0;x<Rx;x++){
  				r=Img[x][y][0];g=Img[x][y][1];b=Img[x][y][2];
    			med=(r+g+b)/3;
				if(med <= 127)
					R[x][y][0]=R[x][y][1]=R[x][y][2]=0;
				else
				  	R[x][y][0]=R[x][y][1]=R[x][y][2]=255;		
			}			
		}
	}
		
}



//---------------------------------------------------------------------------------------------------


//-----------------------------------Transformaciones de Color----------------------------------------

//  Blanco y negro 
void bn()
{
 int x,y;
 int r,g,b, newR, newG, newB,med;

 for (y=0;y<Ry;y++)
  for (x=0;x<Rx;x++)
   {
    r=Img[x][y][0];g=Img[x][y][1];b=Img[x][y][2];
    med=(r+g+b)/3;
  	R[x][y][0]=R[x][y][1]=R[x][y][2]=med;
   }
}

// Sepia
//OK
void sepia(){
	
	int x,y;
	int r,g,b,med;
	
	for (y=0;y<Ry;y++)
		for (x=0;x<Rx;x++)
		{
			r=Img[x][y][0];g=Img[x][y][1];b=Img[x][y][2];
			med=(r+g+b)/3;
			if(med+37 >255)
				R[x][y][0] = 255;
			else
				R[x][y][0] = med+37;
			if(med - 8 <0)
				R[x][y][1] = 0;
			else
			  R[x][y][1] = med-8;
			if(med - 43 <0)
				R[x][y][2] = 0;
			else
			  R[x][y][2] = med-43;
		}
}

//OK
void balanceBlancos(){
	
	int x,y,r1,g1,b1,med,r,g,b =0;
	int balance =0;
	
	for (y=0;y<Ry;y++){
	
  		for (x=0;x<Rx;x++){
		  	
			r=Img[x][y][0];g=Img[x][y][1];b=Img[x][y][2];
			med= (r + g + b)/3;
		
			if(balance < med){
				r1 =r; g1=g; b1=b;
				balance = med;
			}
		}
	}
	
	r1-=255;g1-=255;b1-=255;
		
	for (y=0;y<Ry;y++){
	
		for (x=0;x<Rx;x++){
			
			R[x][y][0] = Img[x][y][0] + r1;
			R[x][y][1] = Img[x][y][1] + g1;
			R[x][y][2] = Img[x][y][2] + b1;
		}
	}
}

//---------------------------------------------------------------------------------------------------



//------Transformaciones Locales------

//TODO
 void suavizadoMedia(){
 	 			
}
 	

//KO
 void suavizadoGaussiano(){
	
}

//OK
 void suavizadoHorizontal(int vecinos){
	
	int x,y,i,acumR,acumG,acumB,n = 0;
	
	
	for (y=0;y<Ry;y++){
	
  		for (x=0;x<Rx;x++){
  			
  			acumR=acumG=acumB=0;
   			if((x >= (vecinos/2))&& (x < Rx - (vecinos/2))){
				
				acumR=acumG=acumB=0;
				for(i= x-(vecinos/2);i < x+(vecinos/2);i++){
					acumR = Img[i][y][0] + acumR;
					acumG = Img[i][y][1] + acumG;
					acumB = Img[i][y][2] + acumB;
				}   
	   			R[x][y][0] = acumR / (vecinos+1);
	   			R[x][y][1] = acumG / (vecinos+1);
			    R[x][y][2] = acumB / (vecinos+1);
		   	}
		   	
		   	
			else if(x < (vecinos/2)){
				i=0;
				n=0;
				while(i < x+(vecinos/2)){
					acumR= acumR+ Img[i][y][0];
					acumG= acumG+ Img[i][y][1];
					acumB= acumB+ Img[i][y][2];
					n++;
					i++;
				}
				R[x][y][0] = acumR / n;
	   			R[x][y][1] = acumG / n;
			    R[x][y][2] = acumB / n;
			}
			else{
				i= x-(vecinos/2);
				n=0;
				while(i < Rx){
					acumR= acumR+ Img[i][y][0];
					acumG= acumG+ Img[i][y][1];
					acumB= acumB+ Img[i][y][2];
					n++;
					i++;
				}
				R[x][y][0] = acumR / n ;
	   			R[x][y][1] = acumG / n ;
			    R[x][y][2] = acumB / n ;
			
			}
		}
	}
}

//TODO
 void suavizadoVertical(int vecinos){
	
	int x,y,i,acumR,acumG,acumB,n = 0;
	
	
	for (y=0;y<Ry;y++){
	
  		for (x=0;x<Rx;x++){
  			
  			acumR=acumG=acumB=0;
   			if((y >= (vecinos/2))&& (y < Ry - (vecinos/2))){
				
				acumR=acumG=acumB=0;
				for(i= y-(vecinos/2);i < y+(vecinos/2);i++){
					acumR = Img[x][i][0] + acumR;
					acumG = Img[x][i][1] + acumG;
					acumB = Img[x][i][2] + acumB;
				}   
	   			R[x][y][0] = acumR / (vecinos+1);
	   			R[x][y][1] = acumG / (vecinos+1);
			    R[x][y][2] = acumB / (vecinos+1);
		   	}
		   	
		   	
			else if(y < (vecinos/2)){
				i=0;
				n=0;
				while(i < y+(vecinos/2)){
					acumR= acumR+ Img[x][i][0];
					acumG= acumG+ Img[x][i][1];
					acumB= acumB+ Img[x][i][2];
					n++;
					i++;
				}
				R[x][y][0] = acumR / n;
	   			R[x][y][1] = acumG / n;
			    R[x][y][2] = acumB / n;
			}
			else{
				i= y-(vecinos/2);
				n=0;
				while(i < Ry){
					acumR= acumR+ Img[x][i][0];
					acumG= acumG+ Img[x][i][1];
					acumB= acumB+ Img[x][i][2];
					n++;
					i++;
				}
				R[x][y][0] = acumR / n ;
	   			R[x][y][1] = acumG / n ;
			    R[x][y][2] = acumB / n ;
			
			}
		}
	}
}

//TODO
void perfilado(){
	
	int x,y,r,g,b,x1,x2,x3,x4,x6,x7,x8,x9,D1,D2,D3,D4,medR,medImg;

	for (y=0;y<Ry;y++){
	
  		for (x=0;x<Rx;x++){
  			
  			r=Img[x-1][y-1][0];g=Img[x-1][y-1][1];b=Img[x-1][y-1][2];
  			x1=(r+g+b)/3;
  			
  			r=Img[x][y-1][0];g=Img[x][y-1][1];b=Img[x][y-1][2];
  			x2=(r+g+b)/3;
  			
  			r=Img[x+1][y-1][0];g=Img[x+1][y-1][1];b=Img[x+1][y-1][2];
  			x3=(r+g+b)/3;
  			
  			r=Img[x-1][y][0];g=Img[x-1][y][1];b=Img[x-1][y][2];
  			x4=(r+g+b)/3;
  			
  			r=Img[x+1][y][0];g=Img[x+1][y][1];b=Img[x+1][y][2];
			x6=(r+g+b)/3;
			
  			r=Img[x-1][y+1][0];g=Img[x-1][y+1][1];b=Img[x-1][y+1][2];
  			x7=(r+g+b)/3;
  		
			r=Img[x][y+1][0];g=Img[x][y+1][1];b=Img[x][y+1][2];
  			x8=(r+g+b)/3;
			
			r=Img[x+1][y+1][0];g=Img[x+1][y+1][1];b=Img[x+1][y+1][2];
			x9=(r+g+b)/3;
			
			D1= ((x7+x8+x9)- (x1+x2+x3));
			D2= ((x6+x8+x9)- (x1+x2+x4));
			D3= ((x3+x6+x9)- (x1+x4+x7));
			D4= ((x1+x2+x4)- (x6+x8+x9));
			
			R[x][y][0]=R[x][y][1]=R[x][y][2] = maxnum(maxnum(maxnum(D1,D2),D3),D4);
		}
	}
	
	for (y=0;y<Ry;y++){
	
  		for (x=0;x<Rx;x++){
  			medR= R[x][y][0];
  			medImg = (Img[x][y][0] + Img[x][y][1] + Img[x][y][2])/3;
  			if(medImg > medR){
  				R[x][y][0] =Img[x][y][0];
  				R[x][y][1] =Img[x][y][1];
  				R[x][y][2] =Img[x][y][2];
			  }
  		}
  }
}

//OK
void bordeadoVertical(){
	
	int x,y,r,g,b,r1,g1,b1,med,med1;
	
		
	for (y=0;y<Ry;y++){
	
  		for (x=0;x<Rx;x++){
  			
  			 r=Img[x][y][0];g=Img[x][y][1];b=Img[x][y][2];
  			 r1=Img[x][y-1][0];g1=Img[x][y-1][1];b1=Img[x][y-1][2];
			med=(r+g+b)/3;
			med1=(r1+g1+b+1)/3;
			R[x][y][0]=R[x][y][1]=R[x][y][2]=(med-med1)/2;
  		}
	}
	
}
//OK
void bordeadoHorizontal(){
	
	int x,y,r,g,b,r1,g1,b1,med,med1;

	
	for (y=0;y<Ry;y++){
	
  		for (x=0;x<Rx;x++){
  			
  			 r=Img[x][y][0];g=Img[x][y][1];b=Img[x][y][2];
  			 r1=Img[x-1][y][0];g1=Img[x-1][y][1];b1=Img[x-1][y][2];
			med=(r+g+b)/3;
			med1=(r1+g1+b+1)/3;
			R[x][y][0]=R[x][y][1]=R[x][y][2]=(med-med1)/2;
  		}
	}
	
	
}
//OK
void bordeadoDiagonal(){
	
	int x,y,r,g,b,r1,g1,b1,med,med1;
	
	
	for (y=0;y<Ry;y++){
	
		for (x=0;x<Rx;x++){
			
			r=Img[x][y][0];g=Img[x][y][1];b=Img[x][y][2];
			r1=Img[x-1][y-1][0];g1=Img[x-1][y-1][1];b1=Img[x-1][y-1][2];
			med=(r+g+b)/3;
			med1=(r1+g1+b+1)/3;
			R[x][y][0]=R[x][y][1]=R[x][y][2]=(med-med1)/4;
		}
	}
	
}
//OK
void bordeadoPrewitt(){
	
	int x,y,r,g,b,x1,x2,x3,x4,x6,x7,x8,x9;
	
	
	for (y=0;y<Ry;y++){
	
  		for (x=0;x<Rx;x++){
  			
  			r=Img[x-1][y-1][0];g=Img[x-1][y-1][1];b=Img[x-1][y-1][2];
  			x1=(r+g+b)/3;
  			
  			r=Img[x][y-1][0];g=Img[x][y-1][1];b=Img[x][y-1][2];
  			x2=(r+g+b)/3;
  			
  			r=Img[x+1][y-1][0];g=Img[x+1][y-1][1];b=Img[x+1][y-1][2];
  			x3=(r+g+b)/3;
  			
  			r=Img[x-1][y][0];g=Img[x-1][y][1];b=Img[x-1][y][2];
  			x4=(r+g+b)/3;
  			
  			r=Img[x+1][y][0];g=Img[x+1][y][1];b=Img[x+1][y][2];
			x6=(r+g+b)/3;
			
			r=Img[x-1][y+1][0];g=Img[x-1][y+1][1];b=Img[x-1][y+1][2];
  			x7=(r+g+b)/3;
			
			r=Img[x][y+1][0];g=Img[x][y+1][1];b=Img[x][y+1][2];
  			x8=(r+g+b)/3;
			
			r=Img[x+1][y+1][0];g=Img[x+1][y+1][1];b=Img[x+1][y+1][2];
			x9=(r+g+b)/3;
			
			R[x][y][0]=R[x][y][1]=R[x][y][2]= (fabs((x3+x6+x9))-fabs((x1+x4+x7))+fabs((x7+x8+x9))-fabs((x1+x2+x3)))/9;
  		}
	}
	
}
//OK
void bordeadoScharr(){
	
	int x,y,r,g,b,x1,x2,x3,x4,x6,x7,x8,x9;

	
	for (y=0;y<Ry;y++){
	
  		for (x=0;x<Rx;x++){
  			
  			r=Img[x-1][y-1][0];g=Img[x-1][y-1][1];b=Img[x-1][y-1][2];
  			x1=(r+g+b)/3;
  			
  			r=Img[x][y-1][0];g=Img[x][y-1][1];b=Img[x][y-1][2];
  			x2=(r+g+b)/3;
  			
  			r=Img[x+1][y-1][0];g=Img[x+1][y-1][1];b=Img[x+1][y-1][2];
  			x3=(r+g+b)/3;
  			
  			r=Img[x-1][y][0];g=Img[x-1][y][1];b=Img[x-1][y][2];
  			x4=(r+g+b)/3;
  			
  			r=Img[x+1][y][0];g=Img[x+1][y][1];b=Img[x+1][y][2];
			x6=(r+g+b)/3;
				  			
  			r=Img[x-1][y+1][0];g=Img[x-1][y+1][1];b=Img[x-1][y+1][2];
  			x7=(r+g+b)/3;
  			
  			r=Img[x][y+1][0];g=Img[x][y+1][1];b=Img[x][y+1][2];
  			x8=(r+g+b)/3;
  			
			r=Img[x+1][y+1][0];g=Img[x+1][y+1][1];b=Img[x+1][y+1][2];
			x9=(r+g+b)/3;
			
			R[x][y][0]=R[x][y][1]=R[x][y][2]=(fabs(3*x3+10*x6+3*x9)-fabs(3*x1+10*x4+3*x7)+fabs((3*x7+10*x8+3*x9))-fabs((3*x1+10*x2+3*x3)))/9;
  		}
	}

}
//OK
void bordeadoSobel(){
	
	int x,y,r,g,b,x1,x2,x3,x4,x6,x7,x8,x9;

	
	for (y=0;y<Ry;y++){
	
  		for (x=0;x<Rx;x++){
  			
  			r=Img[x-1][y-1][0];g=Img[x-1][y-1][1];b=Img[x-1][y-1][2];
  			x1=(r+g+b)/3;
  			
  			r=Img[x][y-1][0];g=Img[x][y-1][1];b=Img[x][y-1][2];
  			x2=(r+g+b)/3;
  			
  			r=Img[x+1][y-1][0];g=Img[x+1][y-1][1];b=Img[x+1][y-1][2];
  			x3=(r+g+b)/3;
  			
  			r=Img[x-1][y][0];g=Img[x-1][y][1];b=Img[x-1][y][2];
  			x4=(r+g+b)/3;
  		
  			r=Img[x+1][y][0];g=Img[x+1][y][1];b=Img[x+1][y][2];
			x6=(r+g+b)/3;
			
			r=Img[x-1][y+1][0];g=Img[x-1][y+1][1];b=Img[x-1][y+1][2];
  			x7=(r+g+b)/3;
			
			r=Img[x][y+1][0];g=Img[x][y+1][1];b=Img[x][y+1][2];
  			x8=(r+g+b)/3;
			
			r=Img[x+1][y+1][0];g=Img[x+1][y+1][1];b=Img[x+1][y+1][2];
			x9=(r+g+b)/3;
			
			R[x][y][0]=R[x][y][1]=R[x][y][2]=(fabs(x3+2*x6+x9)-fabs(x1+2*x4+x7)+fabs((x7+2*x8+x9))-fabs((x1+2*x2+x3)))/9;
  		}
	}
	
	
}
//OK
void bordeadoMaximoGradiente(){
	int x,y,r,g,b,x1,x2,x3,x4,x6,x7,x8,x9,D1,D2,D3,D4;
	
	for (y=0;y<Ry;y++){
	
  		for (x=0;x<Rx;x++){
  			
  			r=Img[x-1][y-1][0];g=Img[x-1][y-1][1];b=Img[x-1][y-1][2];
  			x1=(r+g+b)/3;
  			
  			r=Img[x][y-1][0];g=Img[x][y-1][1];b=Img[x][y-1][2];
  			x2=(r+g+b)/3;
  			
  			r=Img[x+1][y-1][0];g=Img[x+1][y-1][1];b=Img[x+1][y-1][2];
  			x3=(r+g+b)/3;
  			
  			r=Img[x-1][y][0];g=Img[x-1][y][1];b=Img[x-1][y][2];
  			x4=(r+g+b)/3;
  			
  			r=Img[x+1][y][0];g=Img[x+1][y][1];b=Img[x+1][y][2];
			x6=(r+g+b)/3;
			
  			r=Img[x-1][y+1][0];g=Img[x-1][y+1][1];b=Img[x-1][y+1][2];
  			x7=(r+g+b)/3;
  		
			r=Img[x][y+1][0];g=Img[x][y+1][1];b=Img[x][y+1][2];
  			x8=(r+g+b)/3;
			
			r=Img[x+1][y+1][0];g=Img[x+1][y+1][1];b=Img[x+1][y+1][2];
			x9=(r+g+b)/3;
			
			D1= ((x7+x8+x9)- (x1+x2+x3));
			D2= ((x6+x8+x9)- (x1+x2+x4));
			D3= ((x3+x6+x9)- (x1+x4+x7));
			D4= ((x1+x2+x4)- (x6+x8+x9));
			
			R[x][y][0]=R[x][y][1]=R[x][y][2] = maxnum(maxnum(maxnum(D1,D2),D3),D4);
		}
	}
	
}
//------------------------------------

//------Transformaciones geometricas------

void trasladar(int xt, int yt, int xexp, int yexp){
	
	int x,y;

	limpiarImagen();
	for (y=yt;y<yt+yexp;y++){
	
  		for (x=xt;x<xexp;x++){
		
			R[x-xt][y-yt][0] = Img[x][y][0];
			R[x-xt][y-yt][1] = Img[x][y][1];
			R[x-xt][y-yt][2] = Img[x][y][2];
			  
		}
	}

}

//Ok
void inclinarX(double xi){
	
	int x,y,xt=0;
	xi = xi*(-1);
	limpiarImagen();
	for (y=0;y<Ry;y++){
	
  		for (x=0;x<Rx;x++){
  			xt=x-(xi*y);
  			if(xt> 0  && xt< Rx){
				  
				R[xt][y][0]= Img[x][y][0]; 
				R[xt][y][1]= Img[x][y][1]; 
				R[xt][y][2]= Img[x][y][2];
			}

			 
		}
	}

}

//Ok
void inclinarY( double yi){
	
	int x,y,yt=0;
	yi = yi * (-1);
	limpiarImagen();
	for (y=0;y<Ry;y++){
	
  		for (x=0;x<Rx;x++){
			yt=y-(yi*x);
			if( yt>0 && yt< Ry){
				R[x][yt][0]= Img[x][y][0]; 
				R[x][yt][1]= Img[x][y][1]; 
				R[x][yt][2]= Img[x][y][2];
			}

			 
		}
	}
	
}

//Ok
void rotar(int angulo){
	
	
	int x,y,xt,yt;
	
	limpiarImagen();
	for (y=0;y<Ry;y++){
	
  		for (x=0;x<Rx;x++){
		
			xt = (cos(degreesToRadians(angulo))*x) + (-sin(degreesToRadians(angulo)) *y);
			yt = (sin(degreesToRadians(angulo))*x) + (cos(degreesToRadians(angulo)) *y);
			R[xt][yt][0]= Img[x][y][0];
			R[xt][yt][1]= Img[x][y][1];
			R[xt][yt][2]= Img[x][y][2];  
		}
	}
}

void escalarVecinos(double factor){
	
	int x,y=0;
	
	limpiarImagen();
	
	for (y=0;y<Ry;y++){
		
	  	for (x=0;x<Rx;x++){
	  	
	  		R[x][y][0] = Img[(int)round(x*(1/factor))][(int)round(y*(1/factor))][0];
	  		R[x][y][1] = Img[(int)round(x*(1/factor))][(int)round(y*(1/factor))][1];
	  		R[x][y][2] = Img[(int)round(x*(1/factor))][(int)round(y*(1/factor))][2];
	  	
		}
	}
	
}

//KO
void escalarBilineal(double factor){
	
}
//KO
void escalarBicubica(double factor){
	
}

//##Transformaciones de mapeo##

//Ok
void difuminadoAleatorio(int r){
	
	int x,y,xt,yt,random=0;
	
	for (y=0;y<Ry;y++){
		
	  	for (x=0;x<Rx;x++){
	  		random = rand()%(2*r);
	  		xt = x + random - r;
	  		yt = y + random - r;
	  		R[x][y][0] = Img[xt][yt][0];
	  		R[x][y][1] = Img[xt][yt][1];
	  		R[x][y][2] = Img[xt][yt][2];		  	
		}
	}
	
}

//Ok
void pixeladoBloque(int n){
	
	int x,y;
	
	for (y=0;y<Ry;y++){
		
	  	for (x=0;x<Rx;x++){
		  	R[x][y][0] = Img[(x/n)*n][(y/n)*n][0];
			R[x][y][1] = Img[(x/n)*n][(y/n)*n][1];
			R[x][y][2] = Img[(x/n)*n][(y/n)*n][2];
		}
	}
	
}

//Ok
void cristalCuadratico(int n){
	
	int x,y,xt,yt,random=0;
	
	for (y=0;y<Ry;y++){
		
	  	for (x=0;x<Rx;x++){
		
		R[x][y][0] = Img[x - (x%n) + (y%n)][y- (y%n) + (x%n)][0];
		R[x][y][1] = Img[x - (x%n) + (y%n)][y- (y%n) + (x%n)][1];
		R[x][y][2] = Img[x - (x%n) + (y%n)][y- (y%n) + (x%n)][2];
	  	}
	}

}


// ------AYUDAS------

//Comando de ayuda para conocer las posibles funciones
void help(){
 printf("			--------------------------------\n");
 printf("			|  Lista de comandos posibles  |\n");
 printf("			--------------------------------\n");
	printf( "\n##### Transformaciones Globales #####\n\n");
	printf( "	-[P] Para SUMAR un valor a la matriz de colores\n\n");
	printf( "	-[R] Para RESTAR un valor a la matriz de colores\n\n");
	printf( "	-[M] Para MULTIPLICAR la matriz de colores por un valor\n\n");
	printf(	"	-[D] Para DIVIDIR la matriz de colores por un valor\n\n");
	printf(	"	-[I] Para INVERTIR la matriz de colores por un valor\n\n");
	printf( "	-[L] Para generar un AJUSTE LINEAL\n\n");
	printf( "	-[N] Para generar un AJUSTE NO LINEAL\n\n");
	printf( "	-[G] Para la TRANSFORMACION DE GAMA\n\n");
	printf( "	-[Q] Para la ECUALIZACION del histograma\n\n");
	printf( "	-[U] Para aplicar UMBRALIZACOIN introduzca 'U'\n\n");
	printf( "##### Transformaciones de Color #####\n\n");
	printf( "	-[B] Para producir una imagen en BLANCO Y NEGRO introduzca 'B'\n\n");
	printf( "	-[S] Para producir una imagen en SEPIA introduzca 'S'\n\n");
	printf("##### Transformaciones Locales #####\n\n");
	printf("	-[S] Para SUAVIZAR de imagen introduzca: 'X'\n\n");
	printf("	-[P] Para PERFILAR de imagen introduzca: 'Z'\n\n");	
	printf("	-[B] Para PERFILAR de imagen introduzca: 'Z'\n\n");	
	printf("##### Comandos Generales #####\n\n");
	printf(	"	-[H] Para ver la AYUDA\n\n");

	printf(	"	-[E] Para SALIR del programa\n\n");
	printf("	-[A] Para VOLVER ATRAS\n\n");
	
}

void helpGeneral(){
	printf("\n			--------------------------------\n");
 	printf("			|  Lista de comandos posibles  |\n");
 	printf("			|   Tipos de Transformaciones  |\n"); 	
 	printf("			--------------------------------\n");
 	printf("			   Imagen Actual: %s  \n",na);
	printf("			--------------------------------\n\n");	
	printf("			-[1] Transformaciones Globales\n\n");
	printf("			-[2] Transformaciones Locales\n\n");
	printf("			-[3] Transformaciones Geometricas\n\n");
	printf( "			-[C] Para cambiar de imagen\n\n");	
	printf( "			-[X] Para limpiar la consola\n\n");	
	printf(	"			-[H] Para ver la AYUDA\n\n");
	printf(	"			-[E] Para SALIR del programa\n\n");
}

void helpGlobales(){
	printf("\n			--------------------------------\n");
 	printf("			|  Lista de comandos posibles  |\n");
 	printf("			|  Transformaciones Globales   |\n"); 	
 	printf("			--------------------------------\n");
 	printf("			   Imagen Actual: %s  \n",na);
	printf("			--------------------------------\n\n");	
	printf( "\n		##### Transformaciones del Histograma #####\n\n");
	printf( "			-[P] Para SUMAR un valor a la matriz de colores\n\n");
	printf( "			-[R] Para RESTAR un valor a la matriz de colores\n\n");
	printf( "			-[M] Para MULTIPLICAR la matriz de colores por un valor\n\n");
	printf(	"			-[D] Para DIVIDIR la matriz de colores por un valor\n\n");
	printf(	"			-[I] Para INVERTIR la matriz de colores por un valor\n\n");
	printf( "			-[L] Para generar un AJUSTE LINEAL\n\n");
	printf( "			-[N] Para generar un AJUSTE NO LINEAL\n\n");
	printf( "			-[G] Para la TRANSFORMACION DE GAMA\n\n");
	printf( "			-[Q] Para la ECUALIZACION del histograma\n\n");
	printf( "			-[U] Para aplicar UMBRALIZACION\n\n");
	printf( "		##### Transformaciones de Color #####\n\n");
	printf( "			-[B] Para producir una imagen en BLANCO Y NEGRO\n\n");
	printf( "			-[S] Para producir una imagen en SEPIA\n\n");
	printf( "			-[W] Para modificar el BALANCE DE BLANCOS\n\n");
	printf( "		##### Comandos generales#####\n\n");
	printf( "			-[X] Para limpiar la consola\n\n");	
	printf(	"			-[H] Para ver la AYUDA\n\n");
	printf("			-[A] Para VOLVER ATRAS\n\n");
	printf(	"			-[E] Para SALIR del programa\n\n");
}

void helpLocales(){
	printf("\n			--------------------------------\n");
 	printf("			|  Lista de comandos posibles  |\n");
 	printf("			|   Transformaciones Locales   |\n"); 	
 	printf("			--------------------------------\n");
 	printf("			   Imagen Actual: %s  \n",na);
	printf("			--------------------------------\n\n");	
	printf("			-[S] Para SUAVIZAR imagen\n\n");
	printf("			-[P] Para PERFILAR imagen\n\n");	
	printf("			-[B] Para BORDEAR imagen\n\n");
	printf( "		##### Comandos generales#####\n\n");
	printf( "			-[X] Para limpiar la consola\n\n");	
	printf(	"			-[H] Para ver la AYUDA\n\n");
	printf("			-[A] Para VOLVER ATRAS\n\n");
	printf(	"			-[E] Para SALIR del programa\n\n");
}

void helpGeometricas(){
	printf("\n			--------------------------------\n");
 	printf("			|  Lista de comandos posibles  |\n");
 	printf("			| Transformaciones Geometricas |\n"); 	
 	printf("			--------------------------------\n");
 	printf("			   Imagen Actual: %s  \n",na);
	printf("			--------------------------------\n\n");	
	printf("			-[F] Para aplicar Transformaciones Afines\n\n");
	printf("			-[P] Para aplicar Transformaciones Bilineal y Perspectiva\n\n");	
	printf("			-[M] Para aplicar Transformaciones de mapeo\n\n");
 	printf( "		##### Comandos generales#####\n\n");
 	printf( "			-[X] Para limpiar la consola\n\n");	
	printf("			-[H] Para ver la AYUDA\n\n");
	printf("			-[A] Para VOLVER ATRAS\n\n");
	printf("			-[E] Para SALIR del programa\n\n");
	
	
}

//----------------------------------------------------------------------------




						//-------Menus---------------

void menuGlobales(){
	
	int finGlo=0;
	char func;
	int val =0;
	float gama = 0.0;
 	char s[100];
 	double fR,fG,fB=0.0;
	
	while(!finGlo && !fin) {
		fflush(stdin);
		printf("Elige un tipo de transformacion\n(Introduce 'h' para concerlas):\n");
	 	scanf("%c",&func);
	 	switch(func){
	 	case 'p':
	 	case 'P':
	 		printf("Introduzca el valor a sumar:\n");
	 		scanf("%d",&val);
	 		printf("Sumando...\n");
	 		suma(val);
	 		sprintf(s, "-sum-%d", val);
		 	GuardaTga(strcat(s,".tga"));
	 		printf("Sumado OK\nGuardando imagen como: %s\n",res);
	 		printf("---------------------------------------------------------\n");
	 		break;
	 	case 'r':
	 	case 'R':
	 		printf("Introduzca el valor a restar:\n");
	 		scanf("%d",&val);
	 		printf("Restando...\n");
	 		resta(val);
			sprintf(s, "-rest-%d", val);
		 	GuardaTga(strcat(s,".tga"));;
	 		printf("Resta OK\nGuardando imagen como: %s\n",res);
	 		printf("---------------------------------------------------------\n");
	 		break;
	 	case 'm':
	 	case 'M':
	 		printf("Introduzca el valor a multiplicar:\n");
	 		scanf("%d",&val);
	 		printf("Multiplicando...\n");
	 		multiplica(val);
	 		sprintf(s, "-mult-%d", val);
		 	GuardaTga(strcat(s,".tga"));
	 		printf("Multiplicacion OK\nGuardando imagen como: %s\n",res);
	 		printf("---------------------------------------------------------\n");
	 		break;
	 	case 'd':
	 	case 'D':
	 		do{ 
	 			printf("Introduzca el valor a dividir (distinto de 0):\n");
	 			scanf("%d",&val);
	 		}while(val == 0);
	 		printf("Dividiendo...\n");	 		
	 		divide(val);
		 	sprintf(s, "-div-%d", val);
		 	GuardaTga(strcat(s,".tga"));
	 		printf("Division OK\nGuardando imagen como: %s\n",res);
	 		printf("---------------------------------------------------------\n");
	 		break;
	 	case 'i':
	 	case 'I':
	 		printf("Invirtiendo los colores...\n");
	 		invierte();
	 		GuardaTga("-inv.tga");
	 		printf("Invertido OK\nGuardando imagen como: %s\n",res);
	 		printf("---------------------------------------------------------\n");
	 		break;
	 	case 'l':
	 	case 'L':
	 		printf("Aplicando ajuste lineal...\n");
	 		ajusteLineal();
	 		GuardaTga("-ajlin.tga");
	 		printf("Ajuste lineal OK\nGuardando imagen como: %s\n",res);
	 		printf("---------------------------------------------------------\n");
	 		break;
		case 'n':
	 	case 'N':
	 		printf("Lo sentimos, esta seccion aun se encuentra en desarrollo\n");
	 		break;
	 	case 'g':
	 	case 'G':
	 		printf("Introduzca un valor para la gama:\n");
	 		scanf("%f",&gama);
	 		printf("Aplicando transformaci�n de gama...\n");
	 		transformacionGama(gama);
	 		sprintf(s, "-gama-%f", gama);
		 	GuardaTga(strcat(s,".tga"));
	 		printf("Transformacion de gamma OK\nGuardando imagen como: %s\n",res);
	 		printf("---------------------------------------------------------\n");
	 		break;
	 	case 'q':
	 	case 'Q':
	 		printf("Aplicando ecualizacion del histograma..\n");
	 		ecualizacion();
		 	GuardaTga("-eq.tga");
	 		printf("Ecualizacion OK\nGuardando imagen como: %s\n",res);
	 		printf("---------------------------------------------------------\n");
	 		break;
	 	case 'b':
	 	case 'B':
	 		printf("Aplicando transformacion en blanco y negro...\n");
	 		bn();
	 		GuardaTga("-bn.tga");
	 		printf("Blanco y negro OK\nGuardando imagen como: %s\n",res);
	 		printf("---------------------------------------------------------\n");
	 		break;
	 	case 's':
	 	case 'S':
	 		printf("Aplicando transformacion en sepia...");
	 		sepia();
	 		GuardaTga("-sepia.tga");
	 		printf("Sepia OK\nGuardando imagen como: %s\n",res);
	 		printf("---------------------------------------------------------\n");
	 		break;
	 	case 'u':
	 	case 'U':
	 		printf("Introduzca el valor del umbral(Si es igual a 0, se aplicar� umbral binario):\n");
	 		scanf("%d",&val);
	 		printf("Aplicando umbral...\n");
	 		umbral(val);
	 		sprintf(s, "-umbral-%d", val);
		 	GuardaTga(strcat(s,".tga"));
	 		printf("Umbral OK\nGuardando imagen como: %s\n",res);
	 		printf("---------------------------------------------------------\n");
	 		break;
	 	case 'w':
		case 'W':
			printf("Aplicando balance de blancos...\n");
			balanceBlancos();
			sprintf(s, "-BalanceBlancos");
			GuardaTga(strcat(s,".tga"));
			printf("Balance de blancos OK\nGuardando imagen como: %s\n",res);
			printf("---------------------------------------------------------\n");
			break;
	 	case 'h':
	 	case 'H':
	 		helpGlobales();
	 		break;
	 	case 'a':
	 	case 'A':
	 		finGlo= 1;
	 		break;
	 	case 'e':
	 	case 'E':
	 		salir();
	 	case 'x':
	 	case 'X':
	 		system("cls");
	 		break;
	 	default:
	 		printf("No se encontro la orden\n");
	 		break;
		 }
		 

	}
}

void menuLocales(){
	
	int finL=0;
	char func;
	int val=0;
	char s[100];
	
	while(!finL && !fin){
		fflush(stdin);
		printf("Elige un tipo de transformacion\n(Introduce 'h' para concerlas):\n");
	 	scanf("%c",&func);
	 	switch(func){
	 		case 's':
			case 'S':
			printf("	- [M] Para suavizado con la media\n\n");
			printf("	- [G] Para suavizado gaussiano\n\n");
			printf("	- [H] Para suavizado horizontal\n\n");
			printf("	- [V] Para suavizado vertical\n\n");
			fflush(stdin);
			scanf("%c",&func);
			switch(func){
				case 'm':
				case 'M':
					printf("Lo sentimos, esta seccion aun se encuentra en desarrollo\n");
				case 'g':
				case 'G':
					printf("Lo sentimos, esta seccion aun se encuentra en desarrollo\n");
					break;
				case 'h':
				case 'H':
					printf("Introduce el n�mero de vecinos\n");
					scanf("%d", &val);
					printf("Aplicando suavizado...\n");
	 				suavizadoHorizontal(val);
	 				sprintf(s, "-suavizadoHorizontal-%d", val);
		 			GuardaTga(strcat(s,".tga"));
	 				printf("Suavizado horizontal OK\nGuardando imagen como: %s\n",res);
	 				printf("---------------------------------------------------------\n");
					break;
				case 'v':
				case 'V':
					printf("Introduce el n�mero de vecinos\n");
					scanf("%d", &val);
					printf("Aplicando suavizado...\n");
	 				suavizadoVertical(val);
	 				sprintf(s, "-suavizadoVertical-%d", val);
		 			GuardaTga(strcat(s,".tga"));
	 				printf("Suavizado vertical OK\nGuardando imagen como: %s\n",res);
	 				printf("---------------------------------------------------------\n");
				break;	
			}
			break;
		case 'p':
		case 'P':
			printf("Aplicando perfilado...\n");
	 		perfilado();
		 	GuardaTga("-perfilado.tga");
	 		printf("perfilado  OK\nGuardando imagen como: %s\n",res);
	 		printf("---------------------------------------------------------\n");
				
			break;
		case 'b':
		case 'B':
			printf("	- [V] Para bordeado vertical\n\n");
			printf("	- [H] Para bordeado horizontal\n\n");
			printf("	- [D] Para bordeado diagonal\n\n");
			printf("	- [P] Para bordeado de Prewitt\n\n");
			printf("	- [R] Para bordeado de Scharr\n\n");
			printf("	- [S] Para bordeado de Sobel\n\n");
			printf("	- [G] Para bordeado de maximo gradiente\n\n");
			fflush(stdin);
			scanf("%c",&func);
			switch(func){
				case 'v':
				case 'V':
					printf("Aplicando bordeado vertical...\n");
	 				bordeadoVertical();
		 			GuardaTga("-bordeadoVertical.tga");
	 				printf("bordeado vertical OK\nGuardando imagen como: %s\n",res);
	 				printf("---------------------------------------------------------\n");
					break;
				case 'h':
				case 'H':
					printf("Aplicando bordeado horizontal...\n");
	 				bordeadoHorizontal();
		 			GuardaTga("-bordeadoHorizontal.tga");
	 				printf("bordeado Horizontal OK\nGuardando imagen como: %s\n",res);
	 				printf("---------------------------------------------------------\n");
					break;
				case 'd':
				case 'D':
					printf("Aplicando bordeado diagonal...\n");
	 				bordeadoDiagonal();
		 			GuardaTga("-bordeadoDiagonal.tga");
	 				printf("bordeado diagonal OK\nGuardando imagen como: %s\n",res);
	 				printf("---------------------------------------------------------\n");
					break;
	
				case 'p':
				case 'P':
					printf("Aplicando bordeado de Prewitt...\n");
	 				bordeadoPrewitt();
		 			GuardaTga("-bordeadoPrewitt.tga");
	 				printf("bordeado de Prewitt OK\nGuardando imagen como: %s\n",res);
	 				printf("---------------------------------------------------------\n");
					break;
				case 'r':
				case 'R':
					printf("Aplicando bordeado de Scharr...\n");
	 				bordeadoScharr();
		 			GuardaTga("-bordeadoScharr.tga");
	 				printf("bordeado de Scharr OK\nGuardando imagen como: %s\n",res);
	 				printf("---------------------------------------------------------\n");
					break;
				case 's':
				case 'S':
					printf("Aplicando bordeado de Sobel...\n");
	 				bordeadoSobel();
		 			GuardaTga("-bordeadoSobel.tga");
	 				printf("bordeado de Sobel OK\nGuardando imagen como: %s\n",res);
	 				printf("---------------------------------------------------------\n");
					break;
				case 'g':
				case 'G':
					printf("Aplicando bordeado de maximo grandiente...\n");
	 				bordeadoMaximoGradiente();
		 			GuardaTga("-bordeadoMaximoGradiente.tga");
	 				printf("bordeado de maximo gradiente OK\nGuardando imagen como: %s\n",res);
	 				printf("---------------------------------------------------------\n");
					break;
			}
			break;
	 	case 'h':
	 	case 'H':
	 		helpLocales();
	 		break;
	 	case 'a':
	 	case 'A':
	 		finL= 1;
	 		break;
	 	case 'e':
	 	case 'E':
	 		salir();
	 	case 'x':
	 	case 'X':
	 		system("cls");
	 		break;
	 	default:
	 		printf("No se encontro la orden\n");
	 		break;
		 			
		}
	}
	
}

void menuAuxiliar(){
		printf("	- [V] Para utilizar estrategia 'Vecinos mas proximos'\n\n");
		printf("	- [H] Para utilizar interpolacion bilineal\n\n");
		printf("	- [D] Para utilizar interpolaci�n bicubica\n\n");
}

void menuGeometricas(){
	
	int finGeo=0;
	char func;
	int val=0;
	int x,y=0;
	double xi,yi=0.0;
	int xaux,yaux=0;
	double factor=0.0;
	char s[100];
	
	while(!finGeo && !fin){
		fflush(stdin);
		printf("Elige un tipo de transformacion\n(Introduce 'h' para concerlas):\n");
	 	scanf("%c",&func);
		 switch(func){
		case 'f':
		case 'F':
			printf("	- [T] Para trasladar la imagen\n\n");
			printf("	- [S] Para escalar la imagen \n\n");
			printf("	- [X] Para inclinar sobre el eje X la imagen\n\n");
			printf("	- [Y] Para inclinar sobre el eje Y la imagen\n\n");
			printf("	- [R] Para rota la imagen\n\n");
			fflush(stdin);
			scanf("%c",&func);
		 	switch(func){
				case 't':
				case 'T':
					printf("Se considera le punto x=0,y=0; el extremo inferior izquierdo\n");
					printf("Introduce la posici�n X de la traslaci�n\n");
					scanf("%d", &x);
					printf("Introduce la posici�n Y de la traslaci�n\n");
					scanf("%d", &y);
					printf("Ahora es necesario conocer el tama�o de amplitud del recorte\n");
					printf("Introduce la expasion del eje X de la traslaci�n\n");
					scanf("%d", &xaux);
					printf("Introduce la expasion Y de la traslaci�n\n");
					scanf("%d", &yaux);
					printf("Aplicando traslaci�n...\n");
	 				trasladar(x,y,xaux,yaux);
	 				sprintf(s, "-traslacion-%d-%d", x,y);
		 			GuardaTga(strcat(s,".tga"));
	 				printf("Traslacion completada OK\nGuardando imagen como: %s\n",res);
	 				printf("---------------------------------------------------------\n");
					break;			
				case 's':
				case 'S':
					printf("Introduce el factor de escala\n (factor <1 reduce, factor > 1 aumenta)\n");
					scanf("%lf",&factor);
					printf("Elige la estrategia de escala\n");
					menuAuxiliar();
					fflush(stdin);
					scanf("%c",&func);
					switch(func){
						case 'v':
						case 'V':
							printf("Aplicando escala con vecinos mas proximos...\n");
			 				escalarVecinos(factor);
			 				sprintf(s, "-escalarVecinos-%lf", factor);
				 			GuardaTga(strcat(s,".tga"));
			 				printf("Escala con Vecinos mas proximos completada OK\nGuardando imagen como: %s\n",res);
			 				printf("---------------------------------------------------------\n");
							break;
						case 'h':
						case 'H':
							printf("Lo sentimos, esta seccion aun se encuentra en desarrollo\n");
							break;
						case 'd':
						case 'D':
							printf("Lo sentimos, esta seccion aun se encuentra en desarrollo\n");
							break;
					}
					break;
				
				case 'x':
				case 'X':
					printf("Introduce el factor de inclinaci�n para el eje X\n");
					scanf("%lf", &xi);
					printf("Aplicando inclinacionX...\n");
	 				inclinarX(xi);
	 				sprintf(s, "-inclinacionX-%lf", xi);
		 			GuardaTga(strcat(s,".tga"));
	 				printf("InclinacionX completada OK\nGuardando imagen como: %s\n",res);
	 				printf("---------------------------------------------------------\n");
					break;
				case 'y':
				case 'Y':
					printf("Introduce el factor de inclinaci�n para el eje Y\n");
					scanf("%lf", &yi);
					printf("Aplicando inclinacionY...\n");
	 				inclinarY(yi);
	 				sprintf(s, "-inclinacionY-%lf", yi);
		 			GuardaTga(strcat(s,".tga"));
	 				printf("InclinacionY completada OK\nGuardando imagen como: %s\n",res);
	 				printf("---------------------------------------------------------\n");
					break;
				
				case 'r':
				case 'R':

					printf("Introduce el angulo de rotacion\n");
					scanf("%d", &val);
					printf("Aplicando rotacion...\n");
	 				rotar(val);
	 				sprintf(s, "-rotacion-%d-", val);
		 			GuardaTga(strcat(s,".tga"));
	 				printf("rotacion completada OK\nGuardando imagen como: %s\n",res);
	 				printf("---------------------------------------------------------\n");
					break;
			
			}
		
		case 'p':
		case 'P':
			printf("Lo sentimos, esta seccion aun se encuentra en desarrollo\n");
			break;
		case 'm':
		case 'M':
			printf("	- [D] Para difuminacion aleatoria\n\n");
			printf("	- [P] Para pixelado de bloque \n\n");
			printf("	- [C] Para cristal cuadratico\n\n");
			fflush(stdin);
			scanf("%c",&func);
		 	switch(func){
			 	case 'd':
			 	case 'D':
			 		printf("Introduce el radio de difuminaci�n:\n");
			 		scanf("%d", &val);
					printf("Aplicando difuminacion aleatoria...\n");
					difuminadoAleatorio(val);
					sprintf(s, "-difuminacionAleatoria-%d-", val);
		 			GuardaTga(strcat(s,".tga"));
		 			printf("Difuminacion Aleatoria completado OK\nGuardando imagen como: %s\n",res);
	 				printf("---------------------------------------------------------\n");
			 		break;
			 	case 'p':
			 	case 'P':
			 		printf("Introduce el tama�o de bloque:\n");
			 		scanf("%d", &val);
					printf("Aplicando pixelado de bloque aleatoria...\n");
					pixeladoBloque(val);
					sprintf(s, "-pixeladoBloque-%d-", val);
		 			GuardaTga(strcat(s,".tga"));
		 			printf("Pixelado de Bloque completado OK\nGuardando imagen como: %s\n",res);
	 				printf("---------------------------------------------------------\n");
			 		break;
			 	case 'c':
			 	case 'C':
			 		printf("Introduce el modulo del cristal:\n");
			 		scanf("%d", &val);
					printf("Aplicando cristal cuadratico aleatoria...\n");
					cristalCuadratico(val);
					sprintf(s, "-cristalCuadratico-%d-", val);
		 			GuardaTga(strcat(s,".tga"));
		 			printf("Cristal Caudratico completado OK\nGuardando imagen como: %s\n",res);
	 				printf("---------------------------------------------------------\n");
			 		break;
			 }
			break;		
		case 'h':
	 	case 'H':
	 		helpGeometricas();
	 		break;
	 	case 'a':
	 	case 'A':
	 		finGeo= 1;
	 		break;
	 	case 'e':
	 	case 'E':
	 		salir();
	 	case 'x':
	 	case 'X':
	 		system("cls");
	 		break;
	 	default:
	 		printf("No se encontro la orden\n");
	 		break;
		 }		
	}
}


//--------------------------------------------------------------------------------



main()
{
	
 srand(time(NULL)); 
 char func;
 int val=0; 
 char s[100];
 printf("				Transformacion de Imagenes\n");
 printf("			  ------------------------------------\n\n"); 
 printf("			-------------------------------\n");
 printf("			|      Eleccion de imagen     |\n");
 printf("			-------------------------------\n");
 
 printf("Nombre de archivo : ");
 gets(na);
 LeeTga(na);
 printf("Resolucion de %d x %d\n",Rx,Ry);


 
 while(!fin){
 	 helpGeneral();
	 printf("Elige un tipo de transformacion\n(Introduce 'h' para concerlas): \n");
	 scanf("%c",&func);
	 switch(func){
	 	case '1':
	 		helpGlobales();
	 		menuGlobales();
	 		break;
	 	case '2':
	 		helpLocales();
	 		menuLocales();
	 		break;
		case '3':
			helpGeometricas();
			menuGeometricas();
			break;
		case 'c':
		case 'C':
			fflush(stdin);
			printf("Nombre de archivo : ");
 			gets(na);
 			LeeTga(na);
 			printf("Resolucion de %d x %d\n",Rx,Ry);
 			break;
	 	case 'H':
	 	case 'h':
	 		helpGeneral();
	 		break;	
	 	case 'E':
	 	case 'e':
	 		salir();
	 		break;
	 	case 'x':
	 	case 'X':
	 		system("cls");
	 		break;
	 	default:
	 		printf("No se encontro el comando\n");
	 		break;
	 }
	 fflush(stdin);

	 
 }
 printf("			--------------------------------\n");
 printf("			| Programa terminado con exito |\n");
 printf("			--------------------------------\n");
 return 0;
}
