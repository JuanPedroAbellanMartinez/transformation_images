# Transformation Images (Transformaciones de imagen)

## English

  Small *script* developed in `C language` for the application of transformations
  in images in an educational way.

  These transformations are applied without the use of any *library* to know,
  in order to understand the behavior of these functionalities.

  The development is mainly focused on 3 groups of transformations:

    * Global Transformations: Those that are applied to the set of all
      the pixels of the image.

    * Local Transformations: Those that focus on a reduced set
      pixels of an image.
      
    * Geometric Transformations: Those that modify the structure or orientation
      from image.


### How to use in Windows

  An executable called *transformation_images.exe* is associated with the project.

### How to use in Linux

  (Working)

## Spanish

  Pequeño *script* desarrollado en `lenguaje C` para la aplicación de transformaciones
  en imágenes de forma educativa.

  Dichas transformaciones se aplican sin el uso de ninguna *librería* para conocer,
  a fin de entender el comportamiento de dichas funcionalidades.

  El desarrollo se centra principalmente en 3 grandes grupos de transformaciones:

    * Transformaciones Globales: Aquellas que son aplicadas al conjunto de todos
      los pixeles de la imagen.

    * Transformaciones Locales: Aquellas que se centran en un conjunto reducido
      de pixeles de una imagen.

    * Transformaciones Geométricas: Aquellas que modifican la estructura u orientación
      de la imagen.


### Como usar en Windows

  Viene asociado en el proyecto un ejecutable llamada *transformation_images.exe*

### Como usar en Linux

  (Working)
